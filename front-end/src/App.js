import React, { Component } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
const Home = React.lazy(() => import("./pages/home.jsx"));
const Admin = React.lazy(() => import("./pages/admin.jsx"));
const User = React.lazy(() => import("./pages/user.jsx"));

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <React.Suspense fallback={<div>Loading....</div>}>
            <Route exact path="/home" component={Home} />
            <Route exact path="/admin" component={Admin} />
            <Route exact path="/user" component={User} />
            <Route exact path="/">
              <Redirect to="/home" />
            </Route>
          </React.Suspense>
        </Switch>
      </Router>
    );
  }
}

export default App;
