import React, { Component } from "react";
import "../../assests/css/hot_and_types.css";
import hinh1 from "../../assests/image/hinh1-1.png";
import hinh2 from "../../assests/image/hinh1-2.png";
import hinh3 from "../../assests/image/hinh1-3.png";
import hinh4 from "../../assests/image/hinh1-4.png";
import hinh5 from "../../assests/image/hinh1-5.png";
import hinh6 from "../../assests/image/hinh1-6.png";
import hinh7 from "../../assests/image/hinh1-7.png";
import hinh8 from "../../assests/image/hinh1-8.png";
import bed from "../../assests/image/bed.svg";
import bath from "../../assests/image/bath.svg";
import border from "../../assests/image/border.svg";
import expand from "../../assests/image/expand.svg";
import heart from "../../assests/image/heart.svg";
import share from "../../assests/image/share.svg";
import round from "../../assests/image/round.svg";

function Hot() {
    return (
        <div className="hot__wrapper">
            <div className="hot__container">
                <h3 className="hot__title">Bất động sản mới nhất</h3>

                <div className="hot__list">
                    <div className="hot__item">
                        <img src={hinh1} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh2} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh3} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh4} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="hot__list">
                    <div className="hot__item">
                        <img src={hinh5} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh6} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh7} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="hot__item">
                        <img src={hinh8} alt="Nhà ở" className="hot__item-img" />
                        <div className="hot__item-info">
                            <div className="hot__item-price-container">
                                <p className="hot__item-price">
                                    45 triệu/m<sup>2</sup>
                                </p>
                                <div className="hot__item-icon-container">
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={heart} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                    <div className="hot__item-icon-subcontainer">
                                        <img src={share} alt="" className="hot__item-icon-main" />
                                        <img src={round} alt="" className="hot__item-icon-round" />
                                    </div>
                                </div>
                            </div>
                            <h4 className="hot__item-name">Tên dự án</h4>
                            <p className="hot__item-address">Địa chỉ</p>
                            <div className="hot__item-info-room">
                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img src={bed} alt="" className="room__info-item-icon" />
                                </div>

                                <div className="room__info-item">
                                    <span>2</span>
                                    <span className="dot__icon"></span>
                                    <img
                                        src={bath}
                                        alt=""
                                        className="room__info-item-icon room__info-item-icon-bath"
                                    />
                                </div>

                                <div className="room__info-item  room__info-item-expand-container">
                                    <p>
                                        2500m<sup>2</sup>{" "}
                                    </p>
                                    <span className="dot__icon"></span>
                                    <div className="room__info-item-expand">
                                        <img
                                            src={border}
                                            alt=""
                                            className="room__info-item-border"
                                        />
                                        <img
                                            src={expand}
                                            alt=""
                                            className="room__info-item-expand-icon"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="morebutton">
                    <button className="button">Xem thêm</button>
                </div>
            </div>
        </div>
    );
}

export default Hot;