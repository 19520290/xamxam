import React, { Component } from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "../main.css"
import Xemtuoi from "./../../assests/img_advantages/Xemtuoi.png"
import Chiphi from "./../../assests/img_advantages/priceHome.png"
import Tinhtoan from "./../../assests/img_advantages/calculator.png"
import Tuvanphongthuy from "./../../assests/img_advantages/xemtuvi.png"
import Carousel from 'react-elastic-carousel'
import logoPartnerBW from "./../../assests/img_advantages/brand1BW.png"
import logoPartnerFC from "./../../assests/img_advantages/brand1FC.png"
import logoPartnerBW2 from "./../../assests/img_advantages/brand2BW.png"
import logoPartnerFC2 from "./../../assests/img_advantages/brand2FC.png"
import logoPartnerBW3 from "./../../assests/img_advantages/brand3BW.png"
import logoPartnerFC3 from "./../../assests/img_advantages/brand3FC.png"
import logoPartnerBW4 from "./../../assests/img_advantages/brand4BW.png"
import logoPartnerFC4 from "./../../assests/img_advantages/brand4FC.png"
import logoPartnerBW5 from "./../../assests/img_advantages/brand5BW.png"
import logoPartnerFC5 from "./../../assests/img_advantages/brand5FC.png"
import logoPartnerBW6 from "./../../assests/img_advantages/brand6BW.png"
import logoPartnerFC6 from "./../../assests/img_advantages/brand6FC.png"
const breakPoints = [
    { width: 250, itemsToShow: 2 },
    { width: 350, itemsToShow: 3 },
    { width: 500, itemsToShow: 4 },
    { width: 768, itemsToShow: 6 },
];
const Item = (props) => {
    return (
        <div className="item__carousel">{props.children}</div>
    )
}
class Advantage extends Component {
    render() {

        return (
            <Fragment>
                <div className="wrap__advantages">
                    <div className="advantages">
                        <div className="main__utility">
                            <h2>Hỗ trợ tiện ích</h2>
                            <div className="row ">
                                <div className="col">
                                    <img src={Xemtuoi} alt="logo" />
                                    <span>Xem tuổi xây nhà</span>
                                </div>
                                <div className="col">
                                    <img src={Chiphi} alt="logo" />
                                    <span>Chi phí xây nhà</span>
                                </div>
                                <div className="col">
                                    <img src={Tinhtoan} alt="logo" />
                                    <span>Tính toán lãi suất</span>
                                </div>
                                <div className="col">
                                    <img src={Tuvanphongthuy} alt="logo" />
                                    <span>Tư vấn phong thủy</span>
                                </div>
                            </div>
                        </div>
                        <div className=" main__partner">
                            <h2>Các đối tác tiêu biểu</h2>
                            <div className="carousel">
                                <Carousel breakPoints={breakPoints} showArrows={false} pagination={false}>
                                    <Item>
                                        <img src={logoPartnerFC} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                    <Item>
                                        <img src={logoPartnerFC2} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW2} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                    <Item>
                                        <img src={logoPartnerFC3} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW3} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                    <Item>
                                        <img src={logoPartnerFC4} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW4} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                    <Item>
                                        <img src={logoPartnerFC5} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW5} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                    <Item>
                                        <img src={logoPartnerFC6} alt="icon__brand__color" className="logoPartner fullcolor" />
                                        <img src={logoPartnerBW6} alt="icon__brand__blackwhite" className="logoPartner blackwhite" />
                                    </Item>
                                </Carousel>
                            </div>
                        </div>
                        <div className=" main__project">
                            <div className="col">
                                <h3>Bất động sản Hồ Chí Minh</h3>
                                <ul>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận 2</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận 7</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận 9</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Tân Bình</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Bình Thạnh</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Thủ Đức</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận 1</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận 10</Link></li>
                                </ul>
                            </div>
                            <div className="col">
                                <h3>Bất động sản Hà Nội</h3>
                                <ul>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Ba Đình</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Đống Đa</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Hà Đông</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Long Biên</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Nam Từ Liên</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Hoàng Hà</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Hai Bà Trưng</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Mua nhà đất Quận Hoàn Kiếm</Link></li>
                                </ul>
                            </div>
                            <div className="col">
                                <h3>Các dự án nổi bật</h3>
                                <ul>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Dự ấn Vinhomes Quận 2</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Chung cư Safira Khoa Điền</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Vinhomes Central Park</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Chung cư Richmond City</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Dự án Aqua City</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Dự án Ecocity Premia Đắk Lắk</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Dự án Charm City Bình Dương</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Chung cư Vinhomes Riverside</Link></li>
                                </ul>
                            </div>
                            <div className="col">
                                <h3>Nhà đất cho thuê</h3>
                                <ul>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê nhà  Quận Nam Từ Liên</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê nhà Quận 9 </Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê nhà Đông Nai</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê chung cư mini</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê nhà nguyên căn Quận 1</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Cho thuê chung cư</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Nhà trọ Đà Nẵng</Link></li>
                                    <li><i class="fas fa-chevron-right"></i><Link to="#">Thuê nhà Quận Hai Bà Trưng</Link></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Advantage;