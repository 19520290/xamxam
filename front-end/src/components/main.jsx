import React, {Component} from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Intro from '../components/main-user/intro'
import Type from '../components/main-user/type'
import Hot from '../components/main-user/hot'
import Place from '../components/main-user/place'
import News from '../components/main-user/news'
import Advantage from '../components/main-user/advantages'
import './main.css'

class Main extends Component {
    render() {
        return (
            <Fragment>
                <div>
                    <Intro></Intro>
                </div>
                <div>
                    <Type></Type>
                </div>
                <div>
                    <Hot></Hot>
                </div>
                <div>
                    <Place></Place>
                </div>
                <div>
                    <News></News>
                </div>
                <div>
                    <Advantage></Advantage>
                </div>
            </Fragment>
          );
    }
}

export default Main;