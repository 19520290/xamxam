import React, {Component} from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Heading from './main-admin/heading'
import Content from './main-admin/content'
import Footer from './main-admin/footer'

class MainPage extends Component {
    render() {
        return (
            <Fragment>
                <div>
                    <Content></Content>
                </div>
            </Fragment>
          );
    }
}

export default MainPage;