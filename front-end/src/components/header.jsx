import React, { Component, useState } from "react";
import { Fragment } from "react/cjs/react.production.min";
import "./header.css";
import { Link } from "react-router-dom";
import logo from "../assests/user_page/logo.png";
import global from "../assests/user_page/global.png";
import Popup from 'reactjs-popup';
import SignIn from "../components/sign/sign-in"
import SignUp from "../components/sign/sign-up"
var posX = window.innerWidth;
window.onresize = () => {
  posX = window.innerWidth;
}
const navbarItems = [
  {
    displayName: "Nhà đất bán",
    link: "",
    dropDownItems: [
      {
        displayName: "Căn hộ chung cư",
        link: "",
      },
      {
        displayName: "Nhà riêng",
        link: "",
      },
      {
        displayName: "Biệt thự",
        link: "",
      },
      {
        displayName: "Nhà mặt phố",
        link: "",
      },
      {
        displayName: "Đất nền dự án",
        link: "",
      },
      {
        displayName: "Đất",
        link: "",
      },
      {
        displayName: "Loại khác",
        link: "",
      },
    ],
  },
  {
    displayName: "Nhà đất cho thuê",
    link: "",
    dropDownItems: [
      {
        displayName: "Căn hộ chung cư",
        link: "",
      },
      {
        displayName: "Nhà riêng",
        link: "",
      },
      {
        displayName: "Biệt thự",
        link: "",
      },
      {
        displayName: "Nhà mặt phố",
        link: "",
      },
      {
        displayName: "Đất nền dự án",
        link: "",
      },
      {
        displayName: "Đất",
        link: "",
      },
      {
        displayName: "Loại khác",
        link: "",
      },
    ],
  },
  {
    displayName: "Phòng họp",
    link: "",
    dropDownItems: [],
  },
  {
    displayName: "Tin tức",
    link: "",
    dropDownItems: [],
  },
  {
    displayName: "Thông tin",
    link: "",
    dropDownItems: [],
  },
  {
    displayName: "Liên hệ",
    link: "",
    dropDownItems: [],
  },
];
class Header extends Component {
  render() {
    return (
      <Fragment>
        <div className="header-container">
          <ul className="header-col">
            <li className="logo-container">
              <Link to="/">
                <img src={logo} alt="logo" className="logo" />
              </Link>
            </li>
            {navbarItems.map((item, index) => {
              return (
                <li className="header-navbar" key={index}>
                  <Link to={item.link} className="navbar-link">
                    {item.displayName}
                  </Link>
                  <div className="line"></div>
                  <ul className="navbar-hover-list">
                    {item.dropDownItems.map((item, index) => {
                      return (
                        <li key={index}>
                          <Link to={item.link}>{item.displayName}</Link>
                        </li>
                      );
                    })}
                  </ul>
                </li>
              );
            })}
          </ul>
          <ul className="header-col">
            <li>
              <Link to="">
                <img src={global} alt="global" />
              </Link>
            </li>
            <li className="header-navbar login">

              <Popup trigger={<button className="navbar-link">Đăng nhập</button>} modal nested lockScroll>
                {close => <SignIn close={close} />}
              </Popup>
              <div className="line"></div>
            </li>
            <li>
              <Popup trigger={<button className="register-btn">Đăng ký</button>} arrow={false} >
                {close => <SignUp close={close} />}
              </Popup>
            </li>
          </ul>
        </div>
      </Fragment>
    );
  }
}

export default Header;
