import React, {Component} from 'react';
import { Link } from 'react-router-dom'
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dropdown from './dropdown/Dropdown'
import user_image from '../../assests/avatar.jpg'
import ic_logout from '../../assests/ic_logout.png'
import './heading.css'



const curr_user = {
    display_name: 'Tai Nguyen',
    image: user_image
}

const notification_item = [
    {
        "icon": "fas fa-exclamation-circle",
        "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
    },
    {
        "icon": "fas fa-cube",
        "content": "Duis malesuada justo eu sapien elementum, in semper diam posuere"
    },
    {
        "icon": "fas fa-shopping-cart",
        "content": "Donec at nisi sit amet tortor commodo porttitor pretium a erat"
    },
    {
        "icon": "fas fa-exclamation-circle",
        "content": "In gravida mauris et nisi commodo porttitor pretium a erat"
    },
    {
        "icon": "fas fa-shopping-cart",
        "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
    }
]

const user_menu = [
    {
        "content": "Quản lý tin đăng",
        "icon": "fas fa-angle-right margin8"
    },
    {
        "content": "Quản lý tài khoản",
        "icon": "fas fa-angle-right margin8"
    },
    {
        "content": "Thay đổi mật khẩu",
        "icon": "fas fa-angle-right margin8"
    }
]

const renderNotificationItem = (item, index) => (
    <div className="notification-item" key={index}>
        <i className={item.icon}></i>
        <span>{item.content}</span>
    </div>
)

const renderUserToggle = (user) => (
    <div className="topnav__right-user">
        <div className="topnav__right-user__image">
            <img src={user.image} alt="" />
        </div>
        <div className="topnav__right-user__name">
            {user.display_name}
        </div>
        <i class="fas fa-angle-down margin8"></i>
    </div>
)

const renderUserMenu =(item, index) => (
    <Link to='/' key={index}>
        <div className="user-menu">
            <span className="user-menu__item" >{item.content}</span>
            <i className={item.icon}></i>
        </div>
    </Link>
)

class Admin extends Component {
    render() {
        return (
            <Fragment>
                <div class="topnav">
                    <ul class="breadcrumb margin8">
                        <i class="fas fa-angle-right margin8"></i>
                        <li style={{ color: "#A18474" }}>Quản lý người dùng</li>
                        <i class="fas fa-angle-right margin8"></i>
                        <li>Quản lý bài đăng</li>
                    </ul>
                    <div className="topnav__right">
                            <div className="topnav__right-item">
                                {/* dropdown here */}
                                <Dropdown
                                    icon='far fa-bell'
                                    badge='12'
                                    contentData={notification_item}
                                    renderItems={(item, index) => renderNotificationItem(item, index)}
                                    renderFooter={() => <Link to='/'>View All</Link>}/>
                        </div>
                        <div className="topnav__right-item">
                            {/* dropdown here */}
                                <Dropdown
                                    customToggle={() => renderUserToggle(curr_user)}
                                    contentData={user_menu}
                                    renderItems={(item, index) => renderUserMenu(item, index)} 
                                    renderFooter={() => <Link to='/'><hr width="100%" /> <span className="logout">Đăng xuất<img src={ic_logout} alt=""className="ic-logout"/></span>    </Link>}/>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

export default Admin;