import React, { Component, useState, } from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import exit from "../../assests/sign_up/exit.png"
import eyeOn from "../../assests/sign_up/eye_on.png"
import eyeOff from "../../assests/sign_up/eye_off.png"
import './sign-up.css'
const SignUp = ({ close }) => {
    const [passVisibility, setPassVisibility] = useState(eyeOff);
    const [rePassVisibility, setRePassVisibility] = useState(eyeOff);
    const [inputType, setInputType] = useState("password");
    const [reInputType, setReInputType] = useState("password");
    const changePassVisibility = () => {
        if (passVisibility === eyeOn) {
            setPassVisibility(eyeOff);
            setInputType("password");
        }
        if (passVisibility === eyeOff) {
            setPassVisibility(eyeOn);
            setInputType("text");
        }
    }
    const changeRePassVisibility = () => {
        if (rePassVisibility === eyeOn) {
            setRePassVisibility(eyeOff);
            setReInputType("password");
        }
        if (rePassVisibility === eyeOff) {
            setRePassVisibility(eyeOn);
            setReInputType("text");
        }
    }
    return (
        <Fragment >
            <div className="container mx-auto h-auto w-440 relative">
                <div className="header">
                    <span></span>
                    <span className="sign-up-title m-top-28">Đăng ký</span>
                    <button className=" m-top-28" onClick={close}>
                        <img src={exit} />
                    </button>
                </div>
                <div className="information-container relative">
                    <div className="account-detail">Thông tin tài khoản</div>
                    <div className="input-field relative">
                        <input className="input-sign-up required" type="text" required="required">
                        </input>
                        <div className="placeholder">
                            Email <span className="require">*</span>
                        </div>
                    </div>
                    <div className="input-field relative">
                        <input className="input-sign-up required" type={inputType} required="required" />
                        <div className="placeholder">
                            Mật khẩu <span className="require">*</span>
                        </div>
                        <button onClick={changePassVisibility} className="m-right-20">
                            <img src={passVisibility} />
                        </button>
                    </div>
                    <div className="input-field relative">
                        <input className="input-sign-up required" type={reInputType} required="required" />
                        <div className="placeholder">
                            Nhập lại mật khẩu <span className="require">*</span>
                        </div>
                        <button onClick={changeRePassVisibility} className="m-right-20">
                            <img src={rePassVisibility} />
                        </button>
                    </div>
                    <div className="account-detail m-top-16">Thông tin cá nhân</div>
                    <div className="input-field relative">
                        <input className="input-sign-up required" type="text" required="required" />
                        <div className="placeholder">
                            Tên đầy đủ <span className="require">*</span>
                        </div>
                    </div>
                    <div className="input-field relative">
                        <input className="input-sign-up required" type="text" required="required" />
                        <div className="placeholder">
                            Ngày sinh
                        </div>
                    </div>

                    <div className="row-state">

                        <select className="input-field w-48 custom-select">
                            <option>Tỉnh/Thành</option>
                        </select>

                        <select className="input-field w-48 custom-select">
                            <option>Quận/Huyện</option>
                        </select>
                    </div>

                    <div className="input-field relative">
                        <select className="w-full custom-select">
                            <option>Phường/Xã</option>
                        </select>
                    </div>

                    <div className="gender-select m-top-16">
                        <span>
                            <label className="middle m-right-16 opacity-50">Nam</label>
                            <input type="radio" name="gender" />
                        </span>

                        <span>
                            <label className="m-right-16 opacity-50">Nữ</label>
                            <input type="radio" name="gender" />
                        </span>

                        <span>
                            <label className="m-right-16 opacity-50">Khác</label>
                            <input type="radio" name="gender" />
                        </span>
                    </div>

                    <div className="type-select m-top-16 width-85">
                        <span>
                            <label className="m-right-16 opacity-50">Cá nhân</label>
                            <input type="radio" name="type" />
                        </span>

                        <span>
                            <label className="m-right-16 opacity-50">Công ty</label>
                            <input type="radio" name="type" />
                        </span>
                    </div>

                    <div className="input-field relative">
                        <input className="input-sign-up required" type="number" required="required" />
                        <div className="placeholder">
                           CMND/CCCD <span className="require">*</span>
                        </div>
                    </div>

                    <div className="m-top-16">
                        <input type="checkbox" />
                        <label className="m-left-16 opacity-50">Tôi đồng ý ...</label>
                    </div>

                    <button className="w-full sign-up-btn m-top-16">
                        Đăng kí
                    </button>
                    <div className="custom-footer m-top-24 padding-bot-26" >
                        <label className="weight-400">Đã là thành viên!&nbsp;</label>
                        <button className="weight-400 des-color">Đăng nhập</button>
                    </div>

                </div>
            </div>

        </Fragment>
    );

}

export default SignUp;