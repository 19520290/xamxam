import React, { Component, useState } from "react";
import { Fragment } from "react/cjs/react.production.min";
import "./header-user.css";
import {
  BrowserRouter,
  Switch, Route,
  Link, NavLink,
  Axios
} from "react-router-dom";
import axios from "axios";
import logo from "../assests/user_page/logo.png";
import Popup from 'reactjs-popup';
import SignUp from "../components/sign/sign-up"
import Dropdown from '../components/main-admin/dropdown/Dropdown'
import user_image from '../assests/avatar.jpg'
import ic_logout from '../assests/ic_logout.png'

import Cookies from 'js-cookie';


const landForSaleLink = "/home/land-for-sale";
const landForRentLink = "/home/land-for-rent";
const meetingRoomLink = "/home/meeting-room";
const newsLink = "/home/news";
const informationLink = "/home/information";
const contractAddressLink = "/home/contact-address";

const navbarItems = [
  {
    displayName: "Nhà đất bán",
    link: landForSaleLink,
    dropDownItems: [
      {
        displayName: "Bán căn hộ chung cư",
        link: landForSaleLink,
      },
      {
        displayName: "Bán nhà biệt thự, liền kề",
        link: landForSaleLink,
      },
      {
        displayName: "Bán nhà mặt phố",
        link: landForSaleLink,
      },
      {
        displayName: "Bán đất nền, dự án",
        link: landForSaleLink,
      },
      {
        displayName: "Bán đất",
        link: landForSaleLink,
      },
      {
        displayName: "Bán trang trại, khu nghỉ dưỡng",
        link: landForSaleLink,
      },
      {
        displayName: "Bán condotel",
        link: landForSaleLink,
      },
      {
        displayName: "Bán kho, nhà xưởng",
        link: landForSaleLink,
      },
      {
        displayName: "Bán loại bất động sản khác",
        link: landForSaleLink,
      },
    ],
  },
  {
    displayName: "Nhà đất cho thuê",
    link: landForRentLink,
    dropDownItems: [
      {
        displayName: "Cho thuê căn hộ chung cư",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê nhà riêng",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê nhà mặt phố",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê nhà trọ, phòng trọ",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê văn phòng",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê cửa hàng - ki-ốt",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê kho, nhà xưởng, đất",
        link: landForRentLink,
      },
      {
        displayName: "Cho thuê loại bất động sản khác",
        link: landForRentLink,
      },
    ],
  },
  {
    displayName: "Phòng họp",
    link: meetingRoomLink,
    dropDownItems: [],
  },
  {
    displayName: "Tin tức",
    link: newsLink,
    dropDownItems: [
      {
        displayName: "Tin tức tuyển dụng",
        link: "",
      },
      {
        displayName: "Tin tức bất động sản",
        link: "",
      },
    ],
  },
  {
    displayName: "Thông tin",
    link: informationLink,
    dropDownItems: [],
  },
  {
    displayName: "Liên hệ",
    link: contractAddressLink,
    dropDownItems: [],
  },
];

let user = {
  display_name: "Tài Nguyên",
  image: user_image
}

const notification_item = [
  {
      "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
  },
  {
      "content": "Duis malesuada justo eu sapien elementum, in semper diam posuere"
  },
  
  {
      "content": "Donec at nisi sit amet tortor commodo porttitor pretium a erat"
  },
  {
      "content": "In gravida mauris et nisi commodo porttitor pretium a erat"
  },
  {
      "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
  }
]
const like_item = [
  {
      "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
  },
  {
      "content": "Duis malesuada justo eu sapien elementum, in semper diam posuere"
  },
  {
      "content": "Donec at nisi sit amet tortor commodo porttitor pretium a erat"
  },
  {
      "content": "In gravida mauris et nisi commodo porttitor pretium a erat"
  },
  {
      "content": "Curabitur id eros quis nunc suscipit blandit pretium a erat"
  }
]

const user_menu = [
  {
      "content": "Quản lý tin đăng",
      "icon": "fas fa-angle-right margin8"
  },
  {
      "content": "Quản lý tài khoản",
      "icon": "fas fa-angle-right margin8"
  },
  {
      "content": "Thay đổi mật khẩu",
      "icon": "fas fa-angle-right margin8"
  }
]

const renderNotificationItem = (item, index) => (
  <div className="notification-item" key={index}>
      <i className={item.icon}></i>
      <span>{item.content}</span>
  </div>
)
const renderLikeItem = (item, index) => (
  <div className="like-item" key={index}>
      <i className={item.icon}></i>
      <span>{item.content}</span>
  </div>
)

const renderUserToggle = (user) => (
  <li className="topnav__right-user">
  <li>
      <div className="topnav__right-user__image">
          <img src={user.image} alt="" />
      </div>
  </li>
  <li>
          <div className="topnav__right-user__name">
          {user.display_name}
          </div>
  </li>
  <li>
        <i class="fas fa-angle-down margin8"></i>
  </li>
  </li>
)

const renderUserMenu =(item, index) => (
  <div>
        <Link to='/' key={index}>
      <div className="user-menu">
          <span className="user-menu__item" >{item.content}</span>
          <i className={item.icon}></i>
      </div>
  </Link>
  </div>
  
)

var getdata = (user) => {
  const test = Cookies.get('token')
  var j = JSON.parse(atob(test.split('.')[1]))
  console.log('test load ' + j['email'])
  console.log('test id ' + j['id'])
  console.log(j)

  axios
  .get(`${process.env.REACT_APP_API_URL}/api/v1/users/` + j['id'] + '/users')
  .then(res => {
    const data = res.data
    const list = data.map((item) =>({
      "fullname": item.fullName,
      "image": item.image
    }))
    // user.display_name = list[0].fullname
    // user.image = list[0].image
    user.display_name = 'aaaaaaaa'
    user.image = list[0].image
    console.log(list)
    console.log(list[0].fullname)
    console.log(list[0].image)
    console.log(user.display_name)
    return user
  })
  .catch(err => {
    console.log(err)
  });
};
// var getdata = (user) => {
//   // const test = Cookies.get('token')
//   // var j = JSON.parse(atob(test.split('.')[1]))
//   // console.log('test load ' + j['email'])
//   // console.log('test id ' + j['id'])
//   // console.log(test)
//   var j = JSON.parse('{"name": "lala","age": 18,"country": "Việt Nam"}')
//   user.display_name = j.name;
//   console.log('test load ' + j.name)
//   //  console.log('test load ' + curr_user.dislay_name )
//   // console.log(test)
//   return user
// }
  
class HeaderUser extends Component {
  // componentDidMount() {
  //   axios.get('${process.env.REACT_APP_API_URL}/api/v1/user/}')
  //     .then(res => {
  //       const user = res.data;
  //       this.setState({ user });
  //     })
  //     .catch(error => console.log(error));
  // }
  render() {
    // console.log("Name" + getdata(user).display_name);
    user = getdata(user);
    return (
      <Fragment>
        <div className="header-container">
          <ul className="header-col">
            <li className="logo-container">
              <Link to="/user">
                <img src={logo} alt="logo" className="logo" />
              </Link>
            </li>
            {navbarItems.map((item, index) => {
              return (
                <li className="header-navbar" key={index}>
                  <NavLink
                    exact
                    to={item.link}
                    activeClassName="active"
                    className="navbar-link">
                    {item.displayName}
                  </NavLink>
                  <div className="line"></div>
                  <ul className="navbar-hover-list">
                    {item.dropDownItems.map((item, index) => {
                      return (
                        <li key={index} className="drop-list-item">
                          <Link className="drop-link" to={item.link}>{item.displayName}</Link>
                        </li>
                      );
                    })}
                  </ul>
                </li>
              );
            })}
          </ul>
          <ul className="header-col-right">
                    {/* <div className="topnav__right"> */}
                            <ul className="topnav__right-item-like">
                                  {/* dropdown here */}
                                  <Dropdown
                                      icon= 'far fa-heart'
                                      badge='12'
                                      contentData={like_item}
                                      renderItems={(item, index) => renderLikeItem(item, index)}
                                      renderFooter={() => <Link to='/user'>View All</Link>}/>
                          </ul>
                    {/* </div> */}
                            <div className="topnav__right-item-notification">
                                  {/* dropdown here */}
                                  <Dropdown
                                      icon='far fa-bell'
                                      badge='12'
                                      contentData={notification_item}
                                      renderItems={(item, index) => renderNotificationItem(item, index)}
                                      renderFooter={() => <Link to='/user'>View All</Link>}/>
                          </div>
                         <div className="topnav__right-item">
                              {/* dropdown here */}
                              <li>
                              <Dropdown
                                      customToggle={() => renderUserToggle(user)}
                                      contentData={user_menu}
                                      renderItems={(item, index) => renderUserMenu(item, index)} 
                                      renderFooter={() => <Link to='/'><hr width="100%" /> <button className="logout">Log out<img src={ic_logout} alt=""className="ic-logout"/></button>    </Link>}/>
                              </li>                               
                          </div>          
              <Popup trigger={<Link to='/user'><hr width="100%" /> <button className="post-btn">Đăng tin</button>    </Link>} modal lockScroll>
              </Popup>
          </ul>
        </div>
      </Fragment>
    );
  }
}

export default HeaderUser;
