import React, { Component } from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LeftMenu from '../components/left-menu';
import Footer from '../components/main-admin/footer'
import Heading from "../components/main-admin/heading"
const MainPage = React.lazy(() => import('../components/main-page.jsx'));
const Content = React.lazy(() => import('../components/main-admin/content.jsx'));
const Accounts = React.lazy(() => import('../components/main-admin/accounts.jsx'));
const Posts = React.lazy(() => import('../components/main-admin/posts.jsx'));
const News = React.lazy(() => import('../components/main-admin/news.jsx'));
const Information = React.lazy(() => import('../components/main-admin/information.jsx'));
const Profile = React.lazy(() => import('../components/main-admin/profile.jsx'));
const Favourite = React.lazy(() => import('../components/main-admin/favourite.jsx'));
const Password = React.lazy(() => import('../components/main-admin/password.jsx'));
const Notification = React.lazy(() => import('../components/main-admin/notification.jsx'));
const Setting = React.lazy(() => import('../components/main-admin/setting.jsx'));

class Admin extends Component {
    render() {
        return (
            <Fragment>
                {/* <h5>đây là trang của admin</h5> */}

                <div>
                    <div>
                        <LeftMenu></LeftMenu>
                    </div>
                    <div style={{height: "100vh", float: "left",width: "calc(100% - 231px)", background: "#F0EFED"}}> 
                        <div>
                            <Heading></Heading>
                        </div>
                        <div>
                            <Router>
                                <Switch>
                                    <Route exact path="/admin" component={MainPage} />
                                    <Route exact path="/admin/content" component={Content} />
                                    <Route exact path="/admin/posts" component={Posts} />
                                    <Route exact path="/admin/accounts" component={Accounts} />
                                    <Route exact path="/admin/news" component={News} />
                                    <Route exact path="/admin/information" component={Information} />
                                    <Route exact path="/admin/profile" component={Profile} />
                                    <Route exact path="/admin/favourite" component={Favourite} />
                                    <Route exact path="/admin/password" component={Password} />
                                    <Route exact path="/admin/notification" component={Notification} />
                                    <Route exact path="/admin/setting" component={Setting} />
                                </Switch>
                            </Router>
                        </div>
                    </div>
                </div>
                <div>
                    <Footer></Footer>
                </div>
            </Fragment>
        );
    }
}

export default Admin;