import React, {Component} from 'react';
import { Fragment } from 'react/cjs/react.production.min';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import HeaderUser from '../components/header-user'
import Footer from '../components/footer'
import './user.css'
const Main = React.lazy(() => import('../components/main.jsx'));
const LandForSale = React.lazy(() => import('../components/land-for-sale.jsx'));
const LandForRent = React.lazy(() => import('../components/land-for-rent.jsx'));
const MeetingRoom = React.lazy(() => import('../components/meeting-room.jsx'));
const Information = React.lazy(() => import('../components/information.jsx'));
const News = React.lazy(() => import('../components/news.jsx'));
const ContactAddress = React.lazy(() => import('../components/contact-address.jsx'));

class User extends Component {
    render() {
        return (
            <Fragment>                
                <Router>
                <HeaderUser></HeaderUser>
                    <Switch>
                        <Route exact path="/user" component={Main}/>
                        <Route exact path="/home/land-for-sale" component={LandForSale}/>
                        <Route exact path="/home/land-for-rent" component={LandForRent}/>
                        <Route exact path="/home/meeting-room" component={MeetingRoom}/>
                        <Route exact path="/home/information" component={Information}/>
                        <Route exact path="/home/news" component={News}/>
                        <Route exact path="/home/contact-address" component={ContactAddress}/>
                    </Switch>
                </Router>

                <Footer></Footer>
            </Fragment>
          );
    }
}

export default User;
