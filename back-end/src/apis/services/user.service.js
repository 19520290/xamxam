const httpStatus = require('http-status')

const CustomError = require('../../utils/custom-error')
const { User } = require('../models')

const getUsers = async () => {
    return User.find({})
}

const createUser = async (userBody) => {
    if (await User.isEmailTaken(userBody.email)) {
        throw new CustomError('401', 'Email already taken')
    }
    if (await User.isCardNumberTaken(userBody.cardNumber)) {
        throw new CustomError('402', 'Card number already taken')
    }
    return User.create(userBody)
}

const updateUser = async (userID, userBody) => {
    const foundUser = await User.findById(userID)
    if (!foundUser) {
        throw new CustomError(httpStatus.BAD_REQUEST, 'User not found')
    }
    return User.findByIdAndUpdate(userID, userBody, {
        new: true,
    })
}

const deleteUser = async (userID) => {
    const foundUser = await User.findById(userID)
    if (!foundUser) throw new CustomError(httpStatus.NOT_FOUND, 'User not found')
    return User.findByIdAndDelete(userID)
}

const getUserByID = async (userID) => {
    return User.find({ _id: userID })
}
module.exports = {
    getUsers,
    createUser,
    updateUser,
    deleteUser,
    getUserByID
}
