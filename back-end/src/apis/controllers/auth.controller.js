const httpStatus = require('http-status')
const { User } = require('../models')
const jwt = require('jsonwebtoken')
const { OAuth2Client } = require('google-auth-library');
const env = require('../../configs/env')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);

const catchAsync = require('../../utils/catch-async')
const { tokenService, userService, authService } = require('../services')
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));

const login = catchAsync(async (req, res) => {
    const { user } = req
    const token = await tokenService.generateToken(user)
    res.setHeader('Authorization', `Bearer ${token}`)
    res.status(httpStatus.OK).send({ user })
})

const register = catchAsync(async (req, res) => {
    const user = await userService.createUser(req.body)
    const token = await tokenService.generateToken(user)

    const verifyEmailToken = await tokenService.generateVerifyEmailToken(user.email)
    await authService.sendVerificationEmail(verifyEmailToken, user.email)

    res.setHeader('Authorization', `Bearer ${token}`)
    res.status(httpStatus.CREATED).send({
        user: user,
        message: 'Send email verification',
     })
})

const sendVerificationEmail = catchAsync(async (req, res) => {
    const { email } = req.user
    const verifyEmailToken = await tokenService.generateVerifyEmailToken(email)
    await authService.sendVerificationEmail(verifyEmailToken, email)
    res.status(httpStatus.OK).send({
        message: 'Send email verification',
    })
})

const activateEmailToken = catchAsync(async (req, res) => {
    const { token } = req.body
    await authService.activateEmailToken(token)
    res.status(httpStatus.OK).send({
        message: 'Your token has been activated',
    })
})

const authGoogle = catchAsync(async (req, res) => {
    const { idToken } = req.body
    client
    .verifyIdToken({idToken, audience: process.env.GOOGLE_CLIENT_ID })
    .then(response => {
        const {email, name, picture} = response.payload

        User.findOne({ email }).exec((err, user) => {
            if (user) {
                const user_payload = {
                    id: user.id,
                    name: user.fullName,
                    email: user.email,
                    role: user.role,
                }
                const token = jwt.sign(user_payload, env.passport.jwtToken, {
                    expiresIn: env.passport.jwtAccessExpired,
                })
                user.isVerifyEmail = true
                user.save()

                res.setHeader('Authorization', `Bearer ${token}`)
                return res.status(httpStatus.OK).send({ user })

            } else {
                const max = 999999999999
                const min= 100000000
                const result = Math.random()*(max - min) + min
                user = new User({
                    fullName: name,
                    email: email,
                    password: email + '123456',
                    address: null,
                    image: picture,
                    birthday: "2001-01-01",
                    sex: "Nam",
                    cardNumber: String(Math.floor(result)),
                    type: true,
                    role: 'USER',
                    isVerifyEmail: true
                })
                user.save((err, data) => {
                if (err) {
                    return res.status(405).json({
                        error: 'Đăng ký bằng tài khoản google không thành công'
                      });
                }
                const user_payload = {
                    id: data.id,
                    name: data.fullName,
                    email: data.email,
                    role: data.role,
                }
                const token = jwt.sign(user_payload, env.passport.jwtToken, {
                    expiresIn: env.passport.jwtAccessExpired,
                })
                res.setHeader('Authorization', `Bearer ${token}`)
                return res.status(httpStatus.OK).send({ user })

              });
            }
          });
        }
    );
})

const authFacebook = catchAsync(async (req, res) => {
    try {
        const {accessToken, userID} = req.body

        const URL = `https://graph.facebook.com/v4.0/${userID}/?fields=id,name,email,picture&access_token=${accessToken}`

        return (
            fetch(URL, {
              method: 'GET'
            })
              .then(response => response.json())
              .then(response => {
                const {email, name} = response
                const picture = response.picture.data.url

                User.findOne({ email }).exec((err, user) => {
                    if (user) {
                        const user_payload = {
                            id: user.id,
                            name: user.fullName,
                            email: user.email,
                            role: user.role,
                        }
                        const token = jwt.sign(user_payload, env.passport.jwtToken, {
                            expiresIn: env.passport.jwtAccessExpired,
                        })
                        user.isVerifyEmail = true
                        user.save()

                        res.setHeader('Authorization', `Bearer ${token}`)
                        return res.status(httpStatus.OK).send({ user })

                    } else {
                        const max = 999999999999
                        const min= 100000000
                        const result = Math.random()*(max - min) + min

                        user = new User({
                            fullName: name,
                            email: email,
                            password: email + '123456',
                            address: null,
                            image: picture,
                            birthday: "2001-01-01",
                            sex: "Nam",
                            cardNumber: String(Math.floor(result)),
                            type: true,
                            role: 'USER',
                            isVerifyEmail: true
                        })
                        user.save((err, data) => {
                        if (err) {
                          return res.status(405).json({
                            error: 'Đăng ký bằng tài khoản facebook không thành công'
                          });
                        }
                        const user_payload = {
                            id: data.id,
                            name: data.fullName,
                            email: data.email,
                            role: data.role,
                        }
                        const token = jwt.sign(user_payload, env.passport.jwtToken, {
                            expiresIn: env.passport.jwtAccessExpired,
                        })
                        res.setHeader('Authorization', `Bearer ${token}`)
                        return res.status(httpStatus.OK).send({ user })
                      });
                    }
                  });
                })
        )


    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
})

module.exports = {
    login,
    register,
    sendVerificationEmail,
    activateEmailToken,
    authGoogle,
    authFacebook,
}
